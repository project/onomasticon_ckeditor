/**
 * @file
 * The abbr language string definitions.
 */

"use strict"

CKEDITOR.plugins.setLang('nonomasticon', 'en', {
  buttonTitle: 'Insert Abbreviation',
  menuItemTitle: 'Edit Abbreviation',
  dialogTitle: 'Abbreviation Properties',
  dialogAbbreviationTitle: 'Abbreviation',
  dialogExplanationTitle: 'Explanation'
});
